use mongodb::bson::oid::ObjectId;
use serde::{Deserialize, Serialize};

use super::crypto::{generate_alias, generate_hash};

pub trait User {}

pub struct RegisterUserRequest {
    pub email: String,
    pub password: String,
}

pub struct LoginUserRequest {
    pub email: String,
    pub hash: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct Auth {
    pub email: String,
    pub hash: String,
    pub refresh_token: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct PersonalProfile {
    pub title: Option<String>,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct Identity {
    pub personal_profile: PersonalProfile,
    pub auth: Auth,
    pub alias: String,
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct Rates {
    pub hourly: usize,
    pub half_hour: usize,
    pub subscription: usize,
}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct ProfessionalMember {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub _id: Option<ObjectId>,
    pub identity: Identity,
    pub biography: Option<String>,
    pub url: Option<String>,
    pub profile_picture_url: Option<String>,
    pub rates: Rates,
    pub appointments: Vec<ObjectId>, // List of appointment IDs
    pub reviews: Vec<ObjectId>,      // List of review IDs
    pub session_logs: Vec<ObjectId>, // List of section log IDs
    pub subscribers: Vec<ObjectId>,  // List of subsciber IDs
    pub chats: Vec<ObjectId>,        // List of chats IDs
                                     // Billing profile will go here when payments are integrated
}

impl ProfessionalMember {
    pub fn new_with_auth(email: String, password: String) -> Self {
        let mut prof = Self::default();
        prof.identity.auth.email = email;
        prof.identity.auth.hash = generate_hash(password);
        prof.identity.alias = generate_alias();
        prof
    }
}

impl User for ProfessionalMember {}

#[derive(Debug, Clone, Deserialize, Serialize, Default)]
pub struct Subscriber {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub _id: Option<ObjectId>,
    pub identity: Identity,
    pub appointments: Vec<ObjectId>,  // List of appointment IDs
    pub subscriptions: Vec<ObjectId>, // List of subscription IDs
    pub reviews: Vec<ObjectId>,       // List of review IDs
    pub chats: Vec<ObjectId>,         // List of chat IDs
                                      // Billing profile will go here when payments are integrated
}

impl Subscriber {
    pub fn new_with_auth(email: String, password: String) -> Self {
        let mut sub = Self::default();
        sub.identity.auth.email = email;
        sub.identity.auth.hash = generate_hash(password);
        sub.identity.alias = generate_alias();
        sub
    }
}

impl User for Subscriber {}
