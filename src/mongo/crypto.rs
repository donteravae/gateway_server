use argon2::Config;
use rand::{
    distributions::{Alphanumeric, DistString},
    thread_rng, Rng,
};

pub fn generate_alias() -> String {
    let mut rng = thread_rng();
    let alias_id: u32 = rng.gen();
    format!("user{}", alias_id)
}

fn generate_salt() -> String {
    let mut rng = thread_rng();
    Alphanumeric.sample_string(&mut rng, 32)
}

pub fn generate_hash(password: String) -> String {
    argon2::hash_encoded(
        password.as_bytes(),
        generate_salt().as_bytes(),
        &Config::default(),
    )
    .unwrap()
}
