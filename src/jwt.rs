use std::error::Error;

use chrono::Duration;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct AccessTokenClaims {
    aud: String,
    exp: usize,
    iat: usize,
    iss: String,
    sub: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RefreshTokenClaims {
    aud: String,
    exp: usize,
    iat: usize,
    iss: String,
    pub sub: String,
}

pub struct Token(pub String);

pub struct JwtManager;

impl JwtManager {
    pub fn new_access_token(id: &str) -> Result<String, Box<dyn Error + Sync + Send>> {
        let claims: AccessTokenClaims = AccessTokenClaims {
            aud: String::from("horizen.health"),
            exp: (chrono::offset::Utc::now() + Duration::minutes(15)).timestamp() as usize,
            iat: chrono::offset::Utc::now().timestamp() as usize,
            iss: String::from("horizen.health"),
            sub: id.to_string(),
        };
        let token = encode(
            &Header::default(),
            &claims,
            &EncodingKey::from_secret(dotenv::var("ACCESS_TOKEN_SECRET")?.as_ref()),
        )?;
        Ok(token)
    }
    pub fn new_refresh_token(id: &str) -> Result<String, Box<dyn Error + Sync + Send>> {
        let claims: RefreshTokenClaims = RefreshTokenClaims {
            aud: String::from("horizen.health"),
            exp: (chrono::offset::Utc::now() + Duration::days(14)).timestamp() as usize,
            iat: chrono::offset::Utc::now().timestamp() as usize,
            iss: String::from("horizen.health"),
            sub: id.to_string(),
        };
        let token = encode(
            &Header::default(),
            &claims,
            &EncodingKey::from_secret(dotenv::var("REFRESH_TOKEN_SECRET")?.as_ref()),
        )?;
        Ok(token)
    }

    // pub fn decode_access_token(encoded_token: String) -> AccessTokenClaims {}
    pub fn decode_refresh_token(
        encoded_token: String,
    ) -> Result<RefreshTokenClaims, Box<dyn Error + Sync + Send>> {
        Ok(decode::<RefreshTokenClaims>(
            &encoded_token,
            &DecodingKey::from_secret(dotenv::var("REFRESH_TOKEN_SECRET")?.as_ref()),
            &Validation::default(),
        )?
        .claims)
    }
}
