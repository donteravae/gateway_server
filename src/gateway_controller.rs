use async_graphql::http::GraphiQLSource;
use async_graphql_axum::{GraphQLRequest, GraphQLResponse};
use axum::{
    response::{Html, IntoResponse},
    Extension,
};
use tower_cookies::Cookies;

use crate::{
    graphql::{auth::AuthSchema, community::CommunitySchema, therapy::TherapySchema},
    jwt::Token,
};

pub struct GatewayController;

impl GatewayController {
    pub async fn web_app() -> impl IntoResponse {}

    pub async fn auth_services(
        schema: Extension<AuthSchema>,
        cookies: Cookies,
        req: GraphQLRequest,
    ) -> GraphQLResponse {
        let mut req = req.into_inner();
        if let Some(cookie) = cookies.get("hid") {
            req = req.data(Token(cookie.value().to_owned()))
        };
        schema.execute(req).await.into()
    }

    pub async fn therapy_services(
        schema: Extension<TherapySchema>,
        req: GraphQLRequest,
    ) -> GraphQLResponse {
        schema.execute(req.into_inner()).await.into()
    }

    pub async fn community_services(
        schema: Extension<CommunitySchema>,
        req: GraphQLRequest,
    ) -> GraphQLResponse {
        schema.execute(req.into_inner()).await.into()
    }

    pub async fn auth_playground() -> impl IntoResponse {
        Html(GraphiQLSource::build().endpoint("/auth").finish())
    }

    pub async fn therapy_playground() -> impl IntoResponse {
        Html(GraphiQLSource::build().endpoint("/therapy").finish())
    }

    pub async fn community_playground() -> impl IntoResponse {
        Html(GraphiQLSource::build().endpoint("/community").finish())
    }
}
