use std::{env, error::Error};

use mongodb::{
    bson::{doc, oid::ObjectId},
    options::{ClientOptions, IndexOptions, ServerApi, ServerApiVersion},
    Client, IndexModel,
};

use crate::jwt::JwtManager;

use self::models::{Auth, ProfessionalMember, RegisterUserRequest, Subscriber};

mod crypto;
pub mod models;

pub struct DatabaseController {
    db: mongodb::Database,
}

impl DatabaseController {
    pub async fn init() -> Result<Self, Box<dyn Error>> {
        let uri = match env::var("MONGO_URI") {
            Ok(v) => v.to_string(),
            Err(_) => "Error loading env variable".into(),
        };

        let mut client_options = ClientOptions::parse(uri).await?;

        let server_api = ServerApi::builder().version(ServerApiVersion::V1).build();
        client_options.server_api = Some(server_api);

        let client = Client::with_options(client_options)?;

        Ok(Self {
            db: client.database("Users"),
        })
    }

    async fn collection_helper<T>(&self, collection_name: &str) -> mongodb::Collection<T> {
        self.db.collection(collection_name)
    }

    pub async fn does_user_exist(&self, email: &str) -> Result<bool, Box<dyn Error + Send + Sync>> {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;
        let professionals_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;
        if let Some(_) = subscriber_collection
            .find_one(doc! {"identity.auth.email": email}, None)
            .await?
        {
            return Ok(true);
        } else if let Some(_) = professionals_collection
            .find_one(doc! {"identity.auth.email": email}, None)
            .await?
        {
            return Ok(true);
        }

        Ok(false)
    }

    pub async fn register_subscriber(
        &self,
        register_user_request: RegisterUserRequest,
    ) -> Result<(String, String, String), Box<dyn Error + Send + Sync>> {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;

        // Make email and alias fields unique
        let unique_option = IndexOptions::builder().unique(true).build();
        let unique_field = IndexModel::builder()
            .keys(doc! { "identity.auth.email": 1, "identity.alias": 1})
            .options(unique_option)
            .build();
        subscriber_collection
            .create_index(unique_field, None)
            .await?;

        // Insert user into database with generated alias

        let new_subcriber =
            Subscriber::new_with_auth(register_user_request.email, register_user_request.password);

        let result = subscriber_collection
            .insert_one(&new_subcriber, None)
            .await?;
        let id = result.inserted_id.as_object_id().unwrap().to_hex();

        let access_token = JwtManager::new_access_token(&id)?;
        let refresh_token = JwtManager::new_refresh_token(&id)?;

        subscriber_collection
            .update_one(
                doc! {"_id": ObjectId::parse_str(&id)?},
                doc! {"$set": {"identity.auth.refresh_token": &refresh_token}},
                None,
            )
            .await?;

        Ok((access_token, refresh_token, new_subcriber.identity.alias))
    }

    pub async fn register_professional(
        &self,
        register_user_request: RegisterUserRequest,
    ) -> Result<(String, String, String), Box<dyn Error + Send + Sync>> {
        let professional_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;

        // Make email field unique
        let unique_option = IndexOptions::builder().unique(true).build();
        let unique_field = IndexModel::builder()
            .keys(doc! { "identity.auth.email": 1, "identity.alias": 1})
            .options(unique_option)
            .build();
        professional_collection
            .create_index(unique_field, None)
            .await?;

        // Insert user into database with generated alias
        let new_professional = ProfessionalMember::new_with_auth(
            register_user_request.email,
            register_user_request.password,
        );
        let result = professional_collection
            .insert_one(&new_professional, None)
            .await?;

        let id = result.inserted_id.as_object_id().unwrap().to_hex();

        let access_token = JwtManager::new_access_token(&id)?;
        let refresh_token = JwtManager::new_refresh_token(&id)?;

        professional_collection
            .update_one(
                doc! {"_id": ObjectId::parse_str(&id)?},
                doc! {"$set": {"identity.auth.refresh_token": &refresh_token}},
                None,
            )
            .await?;

        Ok((access_token, refresh_token, new_professional.identity.alias))
    }

    pub async fn login_subscriber(
        &self,
        id: &str,
    ) -> Result<(Option<String>, Option<String>, Option<String>), Box<dyn Error + Send + Sync>>
    {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;
        let professional_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;

        let access_token = JwtManager::new_access_token(id)?;
        let refresh_token = JwtManager::new_refresh_token(id)?;

        if let Some(user) = subscriber_collection
            .find_one_and_update(
                doc! {"_id": ObjectId::parse_str(id)?},
                doc! {"$set": {"identity.auth.refresh_token": &refresh_token}},
                None,
            )
            .await?
        {
            return Ok((
                Some(user.identity.alias),
                Some(access_token),
                Some(refresh_token),
            ));
        } else if let Some(user) = professional_collection
            .find_one_and_update(
                doc! {"_id": ObjectId::parse_str(id)?},
                doc! {"$set": {"identity.auth.refresh_token": &refresh_token}},
                None,
            )
            .await?
        {
            return Ok((
                Some(user.identity.alias),
                Some(access_token),
                Some(refresh_token),
            ));
        }

        Ok((None, None, None))
    }

    pub async fn get_user_by_email(
        &self,
        email: &str,
    ) -> Result<(Option<String>, Option<Auth>), Box<dyn Error + Send + Sync>> {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;
        let professional_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;
        if let Some(user) = subscriber_collection
            .find_one(doc! {"identity.auth.email": email}, None)
            .await?
        {
            return Ok((Some(user._id.unwrap().to_hex()), Some(user.identity.auth)));
        } else if let Some(user) = professional_collection
            .find_one(doc! {"identity.auth.email": email}, None)
            .await?
        {
            return Ok((Some(user._id.unwrap().to_hex()), Some(user.identity.auth)));
        }

        Ok((None, None))
    }

    pub async fn get_user_by_refresh_token(
        &self,
        token: &str,
    ) -> Result<(Option<String>, Option<String>), Box<dyn Error + Send + Sync>> {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;
        let professional_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;
        if let Some(user) = subscriber_collection
            .find_one(doc! {"identity.auth.refresh_token": token}, None)
            .await?
        {
            return Ok((Some(user._id.unwrap().to_hex()), Some(user.identity.alias)));
        } else if let Some(user) = professional_collection
            .find_one(doc! {"identity.auth.refresh_token": token}, None)
            .await?
        {
            return Ok((Some(user._id.unwrap().to_hex()), Some(user.identity.alias)));
        }

        Ok((None, None))
    }

    pub async fn does_refresh_token_exist(
        &self,
        token: &str,
    ) -> Result<Option<String>, Box<dyn Error + Send + Sync>> {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;
        let professional_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;

        if let Some(user) = subscriber_collection
            .find_one(doc! {"identity.auth.refresh_token": token}, None)
            .await?
        {
            return Ok(Some(user._id.unwrap().to_hex()));
        } else if let Some(user) = professional_collection
            .find_one(doc! {"identity.auth.refresh_token": token}, None)
            .await?
        {
            return Ok(Some(user._id.unwrap().to_hex()));
        }

        Ok(None)
    }

    pub async fn remove_refresh_token_from_user(
        &self,
        user_id: String,
    ) -> Result<bool, Box<dyn Error + Send + Sync>> {
        let subscriber_collection = self.collection_helper::<Subscriber>("subscribers").await;
        let professional_collection = self
            .collection_helper::<ProfessionalMember>("professionals")
            .await;

        if let Some(_) = subscriber_collection
            .find_one_and_update(
                doc! {"_id": ObjectId::parse_str(&user_id)?},
                doc! {"$set": {"identity.auth.refresh_token": ""}},
                None,
            )
            .await?
        {
            return Ok(true);
        } else if let Some(_) = professional_collection
            .find_one_and_update(
                doc! {"_id": ObjectId::parse_str(&user_id)?},
                doc! {"$set": {"identity.auth.refresh_token": ""}},
                None,
            )
            .await?
        {
            return Ok(true);
        }

        Ok(false)
    }
}
