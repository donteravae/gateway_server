use async_graphql::{EmptySubscription, Schema};

use self::{therapy_mutation::TherapyMutation, therapy_query::TherapyQuery};

mod therapy_mutation;
mod therapy_query;

pub type TherapySchema = Schema<TherapyQuery, TherapyMutation, EmptySubscription>;

pub async fn build_therapy_schema() -> TherapySchema {
    Schema::build(
        TherapyQuery::default(),
        TherapyMutation::default(),
        EmptySubscription,
    )
    .finish()
}
