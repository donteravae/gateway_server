use std::error::Error;

use async_graphql::Object;

#[derive(Default)]
pub struct TherapyQuery;

#[Object(extends)]
impl TherapyQuery {
    pub async fn fetch_member(&self) -> Result<String, Box<dyn Error + Sync + Send>> {
        Ok(String::new())
    }
}