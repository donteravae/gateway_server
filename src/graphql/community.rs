use async_graphql::{EmptySubscription, Schema};

use self::{community_mutation::CommunityMutation, community_query::CommunityQuery};

mod community_mutation;
mod community_query;

pub type CommunitySchema = Schema<CommunityQuery, CommunityMutation, EmptySubscription>;

pub async fn build_community_schema() -> CommunitySchema {
    Schema::build(
        CommunityQuery::default(),
        CommunityMutation::default(),
        EmptySubscription,
    )
    .finish()
}
