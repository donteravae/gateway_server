use async_graphql::Object;

#[derive(Default)]
pub struct AuthQuery;

#[Object(extends)]
impl AuthQuery {
    pub async fn fetch_roles(&self) -> String {
        String::new()
    }
}
