use async_graphql::{Enum, InputObject, SimpleObject};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Eq, PartialEq, Enum)]
pub enum UserKind {
    SUBSCRIBER,
    PROFESSIONAL,
}

#[derive(Debug, Clone, Serialize, Deserialize, InputObject)]
pub struct LoginInput {
    pub email: String,
    pub password: String,
}

#[derive(Debug, Clone, Serialize, Deserialize, InputObject)]
pub struct AuthRegistrationInput {
    pub email: String,
    pub password: String,
    pub kind: UserKind,
}

#[derive(Debug, Clone, Serialize, SimpleObject)]
pub struct UserAccessResponse {
    pub status: u16,
    pub message: String,
    pub payload: Option<UserAccessResponsePayload>,
}

#[derive(Debug, Clone, Serialize, Deserialize, SimpleObject)]
pub struct UserAccessResponsePayload {
    pub alias: String,
    pub token: String,
}
