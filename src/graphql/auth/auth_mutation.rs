use std::error::Error;

use async_graphql::{Context, Object};

use crate::{mongo::DatabaseController, jwt::{JwtManager, RefreshTokenClaims}};

use super::{
    auth_models::{AuthRegistrationInput, LoginInput, UserAccessResponse, UserKind, UserAccessResponsePayload},
    helpers::{
        compare_passwords, generate_professional_registration_response,
        generate_subscriber_registration_response, does_refresh_token_exist, is_refresh_token_in_db, remove_refresh_token, get_user_by_refresh_token,
    },
};

#[derive(Default)]
pub struct AuthMutation;

#[Object]
impl AuthMutation {
    pub async fn register_with_email<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        input: AuthRegistrationInput,
    ) -> Result<UserAccessResponse, Box<dyn Error + Sync + Send>> {
        if input.email.len() < 1 || input.password.len() < 1 {
            return Ok(UserAccessResponse {
                status: 400,
                message: "Email and password required".to_string(),
                payload: None,
            });
        }

        let db = ctx
            .data::<DatabaseController>()
            .expect("Error retrieving database controller from context");

        if db.does_user_exist(&input.email).await? {
            return Ok(UserAccessResponse {
                status: 400,
                message: "User already exists".to_string(),
                payload: None,
            });
        }

        match input.kind {
            UserKind::SUBSCRIBER => {
                return Ok(generate_subscriber_registration_response(
                    ctx,
                    db,
                    input.email,
                    input.password,
                )
                .await?)
            }
            UserKind::PROFESSIONAL => {
                return Ok(generate_professional_registration_response(
                    ctx,
                    db,
                    input.email,
                    input.password,
                )
                .await?)
            }
        }
    }

    pub async fn login<'ctx>(
        &self,
        ctx: &Context<'ctx>,
        login_input: LoginInput,
    ) -> Result<UserAccessResponse, Box<dyn Error + Send + Sync>> {
        if login_input.email.len() < 1 || login_input.password.len() < 1 {
            return Ok(UserAccessResponse {
                status: 400,
                message: "Email and password required".to_string(),
                payload: None,
            });
        }

        let db = ctx
            .data::<DatabaseController>()
            .expect("Error retrieving database controller from context");

        if let (Some(id), Some(user_auth)) = db.get_user_by_email(&login_input.email).await? {
            return Ok(compare_passwords(
                ctx,
                db,
                &id,
                &user_auth.hash,
                login_input.password.as_bytes(),
            )
            .await?);
        } else {
            return Ok(UserAccessResponse {
                status: 401,
                message: "Unauthorized".to_string(),
                payload: None,
            });
        };
    }

    pub async fn logout<'ctx>(
        &self,
        ctx: &Context<'ctx>,
    ) -> Result<UserAccessResponse, Box<dyn Error + Sync + Send>> {
        let refresh_token: String;
        match does_refresh_token_exist(ctx) {
            Ok(token) => refresh_token = token,
            Err(mut response) => {
                response.status = 204;
                return Ok(response);
            }
        }

        match is_refresh_token_in_db(ctx, &refresh_token).await {
            Ok(user_id) => match remove_refresh_token(ctx, user_id).await {
                Ok(successful_response) => return Ok(successful_response),
                Err(failed_response) => return Ok(failed_response),
            },
            Err(response) => return Ok(response),
        }
    }

    pub async fn refresh_auth<'ctx>(
        &self,
        ctx: &Context<'ctx>,
    ) -> Result<UserAccessResponse, Box<dyn Error + Send + Sync>> {
        let encoded_token: String;
        match does_refresh_token_exist(ctx) {
            Ok(token) => encoded_token = token,
            Err(response) => return Ok(response),
        }

        let id: String;
        let alias: String;

        match get_user_by_refresh_token(ctx, &encoded_token).await {
            Ok((user_id, user_alias)) => {
                id = user_id;
                alias = user_alias;
            }
            Err(response) => return Ok(response),
        }

        let claims: RefreshTokenClaims;

        match JwtManager::decode_refresh_token(encoded_token) {
            Ok(refresh_claims) => claims = refresh_claims,
            Err(_) => {
                return Ok(UserAccessResponse {
                    status: 403,
                    message: "Forbidden".to_string(),
                    payload: None,
                })
            }
        }

        if &claims.sub != &id {
            return Ok(UserAccessResponse {
                status: 403,
                message: "Forbidden".to_string(),
                payload: None,
            });
        }

        let access_token = JwtManager::new_access_token(&id)?;

        Ok(UserAccessResponse {
            status: 200,
            message: "Refresh successful".to_string(),
            payload: Some(UserAccessResponsePayload {
                alias: alias,
                token: access_token,
            }),
        })
    }
}
