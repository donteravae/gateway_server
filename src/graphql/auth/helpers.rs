use std::error::Error;

use async_graphql::Context;
use tower_cookies::{
    cookie::{time::Duration, SameSite},
    Cookie,
};

use crate::{
    jwt::Token,
    mongo::{self, models::RegisterUserRequest, DatabaseController},
};

use super::auth_models::{UserAccessResponse, UserAccessResponsePayload};

pub async fn generate_login_response<'ctx>(
    ctx: &Context<'ctx>,
    db: &mongo::DatabaseController,
    user_id: &str,
) -> Result<UserAccessResponse, Box<dyn Error + Send + Sync>> {
    let (alias, access, refresh) = db.login_subscriber(user_id).await?;

    let cookie = Cookie::build("hid", refresh.unwrap())
        .http_only(true)
        .secure(false)
        .max_age(Duration::days(1))
        .same_site(SameSite::Strict)
        .finish()
        .to_string();

    ctx.insert_http_header(axum::http::header::SET_COOKIE, cookie);

    Ok(UserAccessResponse {
        status: 200,
        message: "User login successful".to_string(),
        payload: Some(UserAccessResponsePayload {
            alias: alias.unwrap(),
            token: access.unwrap(),
        }),
    })
}

pub async fn generate_subscriber_registration_response<'ctx>(
    ctx: &Context<'ctx>,
    db: &mongo::DatabaseController,
    email: String,
    password: String,
) -> Result<UserAccessResponse, Box<dyn Error + Send + Sync>> {
    let (access, refresh, alias) = db
        .register_subscriber(RegisterUserRequest { email, password })
        .await
        .expect("user registration error");

    let cookie = Cookie::build("hid", refresh)
        .http_only(true)
        .secure(false)
        .max_age(Duration::days(1))
        .same_site(SameSite::Strict)
        .finish()
        .to_string();

    ctx.insert_http_header(axum::http::header::SET_COOKIE, cookie);

    Ok(UserAccessResponse {
        status: 200,
        message: "New user registered!".to_string(),
        payload: Some(UserAccessResponsePayload {
            alias,
            token: access,
        }),
    })
}

pub async fn generate_professional_registration_response<'ctx>(
    ctx: &Context<'ctx>,
    db: &mongo::DatabaseController,
    email: String,
    password: String,
) -> Result<UserAccessResponse, Box<dyn Error + Send + Sync>> {
    let (access, refresh, alias) = db
        .register_professional(RegisterUserRequest { email, password })
        .await
        .expect("user registration error");

    let cookie = Cookie::build("hid", refresh)
        .http_only(true)
        .secure(false)
        .max_age(Duration::days(1))
        .same_site(SameSite::Strict)
        .finish()
        .to_string();

    ctx.insert_http_header(axum::http::header::SET_COOKIE, cookie);

    Ok(UserAccessResponse {
        status: 200,
        message: "New user registered!".to_string(),
        payload: Some(UserAccessResponsePayload {
            alias,
            token: access,
        }),
    })
}

pub async fn compare_passwords<'ctx>(
    ctx: &Context<'ctx>,
    db: &mongo::DatabaseController,
    id: &str,
    hash: &str,
    password_bytes: &[u8],
) -> Result<UserAccessResponse, Box<dyn Error + Send + Sync>> {
    let pw_match = argon2::verify_encoded(&hash, password_bytes)?;

    if pw_match {
        return Ok(generate_login_response(ctx, db, &id).await?);
    } else {
        return Ok(UserAccessResponse {
            status: 401,
            message: "Unauthorized".to_string(),
            payload: None,
        });
    }
}

pub fn does_refresh_token_exist<'ctx>(ctx: &Context<'ctx>) -> Result<String, UserAccessResponse> {
    match ctx.data::<Token>() {
        Ok(token) => {
            if token.0.len() > 0 {
                return Ok(token.0.to_owned());
            } else {
                return Err(UserAccessResponse {
                    status: 401,
                    message: "Token doesn't exist".to_string(),
                    payload: None,
                });
            }
        }
        Err(_) => {
            return Err(UserAccessResponse {
                status: 401,
                message: "Token doesn't exist".to_string(),
                payload: None,
            })
        }
    }
}

pub async fn is_refresh_token_in_db<'ctx>(
    ctx: &Context<'ctx>,
    token: &str,
) -> Result<String, UserAccessResponse> {
    let db = ctx
        .data::<DatabaseController>()
        .expect("Error retrieving database controller from context");

    match db.does_refresh_token_exist(token).await {
        Ok(token) => {
            if let Some(existing_token) = token {
                return Ok(existing_token);
            }

            return Err(UserAccessResponse {
                status: 204,
                message: "No content".to_string(),
                payload: None,
            });
        }
        Err(_) => {
            let cookie = Cookie::build("hid", "")
                .http_only(true)
                .secure(false)
                .max_age(Duration::days(1))
                .same_site(SameSite::Strict)
                .finish()
                .to_string();

            ctx.insert_http_header(axum::http::header::SET_COOKIE, cookie);

            return Err(UserAccessResponse {
                status: 204,
                message: "No content".to_string(),
                payload: None,
            });
        }
    }
}

pub async fn get_user_by_refresh_token<'ctx>(
    ctx: &Context<'ctx>,
    encoded_token: &str,
) -> Result<(String, String), UserAccessResponse> {
    let db = ctx
        .data::<DatabaseController>()
        .expect("Error retrieving database controller from context");
    let id: Option<String>;
    let alias: Option<String>;

    match db.get_user_by_refresh_token(encoded_token).await {
        Ok((user_id, user_alias)) => {
            id = user_id;
            alias = user_alias;
        }
        Err(_) => {
            return Err(UserAccessResponse {
                status: 500,
                message: "Error finding user by refresh token".to_string(),
                payload: None,
            })
        }
    }

    if id.is_none() || alias.is_none() {
        return Err(UserAccessResponse {
            status: 403,
            message: "Forbidden".to_string(),
            payload: None,
        });
    }

    Ok((id.unwrap(), alias.unwrap()))
}

pub async fn remove_refresh_token<'ctx>(
    ctx: &Context<'ctx>,
    user_id: String,
) -> Result<UserAccessResponse, UserAccessResponse> {
    let db = ctx
        .data::<DatabaseController>()
        .expect("Error retrieving database controller from context");

    match db.remove_refresh_token_from_user(user_id).await {
        Ok(success) => {
            if success {
                return Ok(UserAccessResponse {
                    status: 204,
                    message: "Successful refresh token removal".to_string(),
                    payload: None,
                });
            } else {
                return Err(UserAccessResponse {
                    status: 500,
                    message: "Failed refresh token removal".to_string(),
                    payload: None,
                });
            }
        }
        Err(_) => {
            return Err(UserAccessResponse {
                status: 500,
                message: "Server error. Please try again.".to_string(),
                payload: None,
            })
        }
    };
}
