use async_graphql::{EmptySubscription, Schema};

use crate::mongo::DatabaseController;

use self::{auth_mutation::AuthMutation, auth_query::AuthQuery};

mod helpers;
pub mod auth_models;
mod auth_mutation;
mod auth_query;

pub type AuthSchema = Schema<AuthQuery, AuthMutation, EmptySubscription>;

pub async fn build_auth_schema() -> AuthSchema {
    let db_client = DatabaseController::init()
        .await
        .expect("Error initializing database controller");
    Schema::build(
        AuthQuery::default(),
        AuthMutation::default(),
        EmptySubscription,
    )
    .data(db_client)
    .finish()
}
