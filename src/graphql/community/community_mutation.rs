use std::error::Error;

use async_graphql::Object;

#[derive(Default)]
pub struct CommunityMutation;

#[Object]
impl CommunityMutation {
    pub async fn register_with_email<'ctx>(&self) -> Result<String, Box<dyn Error + Sync + Send>> {
        Ok(String::new())
    }
}
