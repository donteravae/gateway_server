use std::error::Error;

use async_graphql::Object;

#[derive(Default)]
pub struct CommunityQuery;

#[Object(extends)]
impl CommunityQuery {
    pub async fn fetch_member(&self) -> Result<String, Box<dyn Error + Sync + Send>> {
        Ok(String::new())
    }
}
