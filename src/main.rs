use axum::{
    http::{
        header::{ACCEPT, AUTHORIZATION, CONTENT_TYPE},
        HeaderValue, Method,
    },
    routing::{get, post},
    Extension, Router, Server,
};
use dotenv::dotenv;
use std::error::Error;
use tower_cookies::CookieManagerLayer;
use tower_http::cors::CorsLayer;

use crate::{
    gateway_controller::GatewayController,
    graphql::{
        auth::build_auth_schema, community::build_community_schema, therapy::build_therapy_schema,
    },
};

pub mod gateway_controller;
pub mod graphql;
pub mod jwt;
pub mod mongo;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    println!("Welcome to the Horizen Gateway");

    dotenv().ok();

    let cors = CorsLayer::new()
        .allow_headers([AUTHORIZATION, ACCEPT, CONTENT_TYPE])
        .allow_methods([Method::GET, Method::POST])
        .allow_credentials(true)
        .allow_origin("http://localhost:3000".parse::<HeaderValue>()?);

    let app = Router::new()
        .route("/", get(GatewayController::web_app))
        .route(
            "/auth",
            post(GatewayController::auth_services).get(GatewayController::auth_playground),
        )
        .route(
            "/therapy",
            post(GatewayController::therapy_services).get(GatewayController::therapy_playground),
        )
        .route(
            "/community",
            post(GatewayController::community_services)
                .get(GatewayController::community_playground),
        )
        .layer(Extension(build_auth_schema().await))
        .layer(Extension(build_therapy_schema().await))
        .layer(Extension(build_community_schema().await))
        .layer(cors)
        .layer(CookieManagerLayer::new());

    println!("GraphiQL IDE: http://localhost:8000/auth");
    println!("GraphiQL IDE: http://localhost:8000/therapy");
    println!("GraphiQL IDE: http://localhost:8000/community");

    Server::bind(&"[::1]:8000".parse()?)
        .serve(app.into_make_service())
        .await?;

    Ok(())
}
